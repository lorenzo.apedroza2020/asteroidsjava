import java.awt.*;
import java.awt.Rectangle;


public class Bullet extends BaseVectorShape {
  public Rectangle getBounds(){
    return new Rectangle((int)getX(), (int)getY(), 1, 1);
  }

  Bullet(){
    setShape(new Rectangle(0, 0, 1, 1));
    setAlive(false);
  }
}
